import cv2
import numpy as np

# empty function for trackbars, can be changed if there is need to
def nothing(x):
    pass


# capturing image from "0th" camera
cap = cv2.VideoCapture(0)

# named window for sliders
cv2.namedWindow("sliders")
cv2.namedWindow("colors")
cv2.namedWindow("edges")

# creating sliders
cv2.createTrackbar("H", "sliders", 94, 179, nothing)
cv2.createTrackbar("S", "sliders", 158, 255, nothing)
cv2.createTrackbar("V", "sliders", 53, 255, nothing)
cv2.createTrackbar("H2", "sliders", 130, 179, nothing)
cv2.createTrackbar("S2", "sliders", 255, 255, nothing)
cv2.createTrackbar("V2", "sliders", 255, 255, nothing)
cv2.createTrackbar("T1", "edges", 70, 255, nothing)
cv2.createTrackbar("T2", "edges", 98, 255, nothing)
cv2.createTrackbar("Area", "edges", 1000, 10000, nothing)

# img read
# raw = cv2.imread("zdj/test.JPG")
# dim = (600, 400)
# original = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)


# main capture loop
while True:
    # original image
    _, raw = cap.read()

    dim = (400, 300)
    original = cv2.resize(raw, dim, interpolation=cv2.INTER_AREA)
    grayscale = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
    result = original.copy()
    # conversion to hsv
    hsv = cv2.cvtColor(original, cv2.COLOR_BGR2HSV)

    # openCV hsv ranges = [0:179, 0:255, 0:255]

    # # fixed range
    # lower = np.array([90, 170, 100])
    # upper = np.array([140, 255, 255])

    # color image
    colors_hsv = np.zeros((150, 300, 3), np.uint8)

    # dynamic ranges
    # lower vals
    hLower = cv2.getTrackbarPos("H", "sliders")
    sLower = cv2.getTrackbarPos("S", "sliders")
    vLower = cv2.getTrackbarPos("V", "sliders")
    lower = np.array([hLower, sLower, vLower])

    # upper vals
    hUpper = cv2.getTrackbarPos("H2", "sliders")
    sUpper = cv2.getTrackbarPos("S2", "sliders")
    vUpper = cv2.getTrackbarPos("V2", "sliders")
    upper = np.array([hUpper, sUpper, vUpper])

    colors_hsv[:, :150] = np.array([hLower, 255, 255])
    colors_hsv[:, 150:] = np.array([hUpper, 255, 255])
    colors = cv2.cvtColor(colors_hsv, cv2.COLOR_HSV2BGR)

    # hsv color reader
    # print(hsv[300, 300])
    # original[300, 300] = [255, 255, 255]

    # creating color mask
    mask = cv2.inRange(hsv, lower, upper)

    # applying mask to gray-scaled image
    filtered = cv2.bitwise_and(grayscale, grayscale, mask=mask)

    # edge detection
    t1 = cv2.getTrackbarPos("T1", "edges")
    t2 = cv2.getTrackbarPos("T2", "edges")
    imgCanny = cv2.Canny(filtered, threshold1=t1, threshold2=t2)

    # kernel
    kernel = np.ones((5, 5), np.uint8)

    # dilation (optional)
    imgDil = cv2.dilate(imgCanny, kernel, iterations=1)

    contours, hierarchy = cv2.findContours(imgDil, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    min_area = cv2.getTrackbarPos("Area", "edges")
    for cnt in contours:
        area = cv2.contourArea(cnt)
        if area > min_area:
            cv2.drawContours(result, cnt, -1, (0, 255, 255), 5)
            perimeter = cv2.arcLength(cnt, True)
            approximation = cv2.approxPolyDP(cnt, 0.02*perimeter, True)
            x, y, width, height = cv2.boundingRect(approximation)
            # if width >= height:
            #     height = width
            # else:
            #     width = height
            cv2.rectangle(result, (x, y), (x+width, y+height), (255, 255, 255), 5)
            cv2.putText(result, "A: " + str(area), (x+width//2, y + height + 40),
                        cv2.FONT_HERSHEY_PLAIN, 2, (255, 255, 255), 3)

    cv2.imshow("original", original)
    cv2.imshow("filtered", filtered)
    cv2.imshow("edge", imgCanny)
    cv2.imshow("result", result)
    cv2.imshow("colors", colors)

    if cv2.waitKey(1) & 0xFF == ord("q"):
        break

cap.release()
cv2.destroyAllWindows()
