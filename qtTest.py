# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!

from PIL import Image
from PIL.ImageQt import ImageQt
from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(757, 560)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalSlider_R = QtWidgets.QSlider(self.centralwidget)
        self.horizontalSlider_R.setGeometry(QtCore.QRect(580, 50, 160, 22))
        self.horizontalSlider_R.setMaximum(255)
        self.horizontalSlider_R.setPageStep(5)
        self.horizontalSlider_R.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider_R.setObjectName("horizontalSlider_R")
        self.horizontalSlider_G = QtWidgets.QSlider(self.centralwidget)
        self.horizontalSlider_G.setGeometry(QtCore.QRect(580, 130, 160, 22))
        self.horizontalSlider_G.setMaximum(255)
        self.horizontalSlider_G.setPageStep(5)
        self.horizontalSlider_G.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider_G.setObjectName("horizontalSlider_G")
        self.horizontalSlider_B = QtWidgets.QSlider(self.centralwidget)
        self.horizontalSlider_B.setGeometry(QtCore.QRect(580, 210, 160, 22))
        self.horizontalSlider_B.setMaximum(255)
        self.horizontalSlider_B.setPageStep(5)
        self.horizontalSlider_B.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider_B.setObjectName("horizontalSlider_B")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(580, 30, 55, 16))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(580, 110, 55, 16))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(580, 190, 55, 16))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.horizontalSlider_R_2 = QtWidgets.QSlider(self.centralwidget)
        self.horizontalSlider_R_2.setGeometry(QtCore.QRect(580, 70, 160, 22))
        self.horizontalSlider_R_2.setMaximum(255)
        self.horizontalSlider_R_2.setPageStep(5)
        self.horizontalSlider_R_2.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider_R_2.setObjectName("horizontalSlider_R_2")
        self.horizontalSlider_R_2.setValue(255)
        self.horizontalSlider_G_2 = QtWidgets.QSlider(self.centralwidget)
        self.horizontalSlider_G_2.setGeometry(QtCore.QRect(580, 150, 160, 22))
        self.horizontalSlider_G_2.setMaximum(255)
        self.horizontalSlider_G_2.setPageStep(5)
        self.horizontalSlider_G_2.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider_G_2.setObjectName("horizontalSlider_G_2")
        self.horizontalSlider_G_2.setValue(255)
        self.horizontalSlider_B_2 = QtWidgets.QSlider(self.centralwidget)
        self.horizontalSlider_B_2.setGeometry(QtCore.QRect(580, 230, 160, 22))
        self.horizontalSlider_B_2.setMaximum(255)
        self.horizontalSlider_B_2.setPageStep(5)
        self.horizontalSlider_B_2.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider_B_2.setObjectName("horizontalSlider_B_2")
        self.horizontalSlider_B_2.setValue(255)
        self.label_red = QtWidgets.QLabel(self.centralwidget)
        self.label_red.setGeometry(QtCore.QRect(670, 30, 65, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_red.setFont(font)
        self.label_red.setObjectName("label_red")
        self.label_green = QtWidgets.QLabel(self.centralwidget)
        self.label_green.setGeometry(QtCore.QRect(670, 110, 65, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_green.setFont(font)
        self.label_green.setObjectName("label_green")
        self.label_blue = QtWidgets.QLabel(self.centralwidget)
        self.label_blue.setGeometry(QtCore.QRect(670, 190, 65, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_blue.setFont(font)
        self.label_blue.setObjectName("label_blue")
        self.image = QtWidgets.QLabel(self.centralwidget)
        self.image.setGeometry(QtCore.QRect(20, 20, 541, 471))
        self.image.setText("")
        self.image.setScaledContents(True)
        self.image.setObjectName("image")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 757, 26))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        # my code
        self.bitmap = Image.open("img/test.jpg")
        self.bmp = Image.open("img/test.jpg")
        self.horizontalSlider_R.valueChanged.connect(lambda: self.valuechange_slider(self.horizontalSlider_R,
                                                                                     self.horizontalSlider_R,
                                                                                     self.horizontalSlider_R_2,
                                                                                     self.label_red))

        self.horizontalSlider_R_2.valueChanged.connect(lambda: self.valuechange_slider(self.horizontalSlider_R_2,
                                                                                       self.horizontalSlider_R,
                                                                                       self.horizontalSlider_R_2,
                                                                                       self.label_red))

        self.horizontalSlider_G.valueChanged.connect(lambda: self.valuechange_slider(self.horizontalSlider_G,
                                                                                     self.horizontalSlider_G,
                                                                                     self.horizontalSlider_G_2,
                                                                                     self.label_green))

        self.horizontalSlider_G_2.valueChanged.connect(lambda: self.valuechange_slider(self.horizontalSlider_G_2,
                                                                                       self.horizontalSlider_G,
                                                                                       self.horizontalSlider_G_2,
                                                                                       self.label_green))

        self.horizontalSlider_B.valueChanged.connect(lambda: self.valuechange_slider(self.horizontalSlider_B,
                                                                                     self.horizontalSlider_B,
                                                                                     self.horizontalSlider_B_2,
                                                                                     self.label_blue))

        self.horizontalSlider_B_2.valueChanged.connect(lambda: self.valuechange_slider(self.horizontalSlider_B_2,
                                                                                       self.horizontalSlider_B,
                                                                                       self.horizontalSlider_B_2,
                                                                                       self.label_blue))

        self.show_image()

    def show_image(self):
        self.qimage = ImageQt(self.bitmap)
        self.image.setPixmap(QtGui.QPixmap.fromImage(self.qimage))
        self.image.show()

    def edit_image(self):
        original = self.bmp.load()
        pix = self.bitmap.load()
        for x in range(self.bitmap.size[0]):
            for y in range(self.bitmap.size[1]):
                r, g, b = original[x, y]
                if self.match_pixels(r, g, b):
                    pix[x, y] = original[x, y]
                else:
                    pix[x, y] = (255, 255, 255, 255)

    def match_pixels(self, r, g, b):
        if self.horizontalSlider_R.value() <= r <= self.horizontalSlider_R_2.value() and \
                self.horizontalSlider_G.value() <= g <= self.horizontalSlider_G_2.value() and \
                self.horizontalSlider_B.value() <= b <= self.horizontalSlider_B_2.value():
            return True
        else:
            return False

    def valuechange_slider(self, sender, slider1, slider2, label):
        if sender == slider1:
            if slider1.value() > slider2.value():
                slider2.setValue(slider1.value())
        else:
            if slider2.value() < slider1.value():
                slider1.setValue(slider2.value())

        label.setText(f"({slider1.value()}, {slider2.value()})")
        label.adjustSize()
        self.edit_image()
        self.show_image()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "WSColorPicker"))
        self.label.setText(_translate("MainWindow", "Red"))
        self.label_2.setText(_translate("MainWindow", "Green"))
        self.label_3.setText(_translate("MainWindow", "Blue"))
        self.label_red.setText(_translate("MainWindow", "(0, 255)"))
        self.label_green.setText(_translate("MainWindow", "(0, 255)"))
        self.label_blue.setText(_translate("MainWindow", "(0, 255)"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
